\section{Evaluation}\label{evaluation}
\subsection{Design}
The design of the evaluation compares the implemented software to an interactive data table, a commonly used method of modelling processes with the keystroke level model. 

\subsubsection{User Tasks}
In order to compare the implemented software to the other method of using keystroke level model, users complete three different tasks.

\subsubsection*{Task 1: Model a Known Process} \label{task1}
During this task, the user models a given process while being able to navigate live the process they are modelling. The first part of this task contains the user attempting to complete the task using the data table, by counting the number of times they use every operator available. Afterwards, the user attempts to complete this task in the software by adding nodes to the graph which sequentially depict the given user actions in the process. The goal of this task is to measure the time needed to model the process.\\

The process users need to model in order to complete this task is the current Amazon online checkout, while also changing the chosen delivery address with another one currently available in the system.

This task has some assumptions. Users are aware of all of them. The first assumption is that the the user is already logged in on Amazon. The second assumption is that they already have in their shopping cart the product they wish to order. The third assumption is that the user has multiple delivery addresses available to choose from already stored in their account. The fourth assumption is that there is no other missing data which would hinder the process being successful in any way.

Users start on the home page of Amazon. Currently, completing a checkout while changing the delivery address requires six sequential clicks, under the previously stated assumptions. This process was chosen in order to allow users to get accustomed to the data table and the software, due to its simplicity.

\subsubsection*{Task 2: Improve a Process with Assistance} \label{task2}
During the second task, the user changes the model out of the first task according to information given prior to the task. After receiving a status quo model which depicts the same process from the first task, the evaluatee receives information regarding the way how they can improve the process. This information contains the location of the possible improvement in the model as well as the possible node types which might improve the process without breaking it. Similar to the first task, the user first competes this task in the data table and afterwards in the software. In order to hinder inconsistencies, users are provided with the same status quo model, regardless of their graph out of the first task. The time needed to model the new design will be measured during this task.\\

This evaluation task makes the same assumptions as the first evaluation task.

Users are being told beforehand, that they should use a dropdown keystroke level model operator in order to choose their new delivery address, in opposition to clicking themselves through as during the status quo. They are asked to imagine replacing the appropriate operators in the data table and in the software with a dropdown operator and check their improvement afterwards using the review panel.

\subsubsection*{Task 3: Improve a Process without Assistance} \label{task3}
At the beginning of this task, the user receives a status quo model and is asked to improve said model by at least 15\%. In contrast to the second task, the user does not receive any information regarding possible improvements, besides the minimum percent of improvement which they must achieve in order to complete the task. The time needed to achieve this percentage is measured. The number of users which achieve a bigger improvement and the number of users which fail to complete the task will also be counted. An average of the improvement will be made on both evaluation groups. Users fail to complete this task if they are not able to achieve the minimal improvement in ten minutes.\\

Users are being explicitly told they do not have any user interface during this task and can make only one assumption regarding the process. In order to generalize the process, users are being told they access the online shop where they have to complete the order using a direct product page link. This means, they can directly add the wanted product to the cart without having to search for it on the website.
 They are asked to first analyse the provided status quo graph and provide another shorter variation of the same process, while achieving the same milestones. The milestones are logging in, adding a product to cart, having some kind of delivery and payment addresses and ordering.

\subsubsection{System Usability Scale} \label{sus}
After completing all three tasks, every user will be required to assess the software using the system usability scale \cite{brooke1996sus}.

The ten-item usability scale is an industry standard and very easy for participants to understand and complete. It consists of ten questions which are scaled using a five point Likert scale. The purpose of administering the system usability scale is to assess how feasible the software is to solving a given task. After completing the three evaluation user tasks, it is assumed that the users have gained enough experience in order to be able to answer the system usability scale questions.

\subsubsection{User Groups}
The evaluation of the software focuses on two different user groups. These groups concentrate on user-based and expert-based evaluation methods, but not on the model-based evaluation \cite{dillon2001evaluation}.

Firstly, the expert user group comprises individuals which work in the field of usability and design, or improve processes frequently. They are the main target group of the software. 

The second test group, the non-experts, are randomly chosen individuals, in  disregard of their computer proficiency or relation to the usability field. Whilst unclear if the participants in the test group possess knowledge regarding usability or process modelling, they should be able to use the software due to its simplicity.

Since all evaluation participant take part in every condition, this is a within group study design \cite{charness2012experimental}. Therefore, individual differences are reduced between both groups. The expert and non-expert groups are also quantified separately. By setting a fixed order to the tasks, users may at most experience some kind of fatigue effect. Users should not experience any kind of practice effect, besides the desired experience effect, which counteracts the increasing difficulty of each task.

\subsection{Evaluation Process}
All evaluation subjects take part in trials individually and under supervision. The order of the evaluation tasks is identical for both groups, so that the participants become more accustomed to the software during each task. The entire evaluation is designed to challenge the evaluation participants more during each task and has an increasing complexity and difficulty in regard of mental and user steps required to complete the tasks.

During the first two task, the user will firstly complete the task using the status quo keystroke level model method, a data table. The reason for starting each time with the data table method is that this method provides less information about how the process was improved. The user has no access to the data table after completing the task and carrying on to the task using the software. While completing the task using the data table method, users may use provided sheets of paper and writing utensils. The third task focuses on the software and will not be completed using the data table. Due to the lack of assistance or user interface during the third task, the information provided in the data table would not be enough for users to solve this task by not compromising the process. 

Evaluation subjects will be asked to think aloud during task execution, but they are not recorded. The evaluation supervisor takes notes of the general assessment of the workflow during the tasks, as well as frustration or other negative emotions due to usability or concept mishaps in the software. Other positive or neutral reactions which happen during the evaluation tasks will be noted as well. After completing the system usability scale questionnaire, the evaluatee will be informally questioned in order to provide more information regarding their previous reactions during using the software. This data will be presented in a further subsection, as well as analysed and discussed.

\subsection{Results}

During the evaluation, 14 users were part of the non-expert user group. In the non-expert group, there were six men and eight women. The average age in the non-expert group was 25.5 years. Non-expert users were informally asked to rate their experience regarding usability in years, which averaged to 2.4 years.

The expert user group consisted out of two evaluatees. Both expert evaluatees identified as male. 

\subsubsection*{Results of Task 1: Model a Known Process}

\paragraph{Non-Experts.} During the first task, the non-expert user group achieved an average of 01:39 minutes needed to completing the task using the data table. Using the software, users achieved the task successfully in 01:17 minutes, on average. The lowest time recorded was of 48 seconds using the data table and 52 seconds using the software. The highest time recorded using the data table was 02:26 minutes and 01:48 minutes using the software. All non-expert users achieved the first task in both the data table and the software successfully.

\paragraph{Experts.} Expert users needed 01:34, respectively 01:56 minutes to complete the task using the data table successfully. By using the software, they successfully completed the modelling task in 59 seconds, respectively 01:33 minutes.

\subsubsection*{Results of Task 2: Improve a Process with Assistance}

\paragraph{Non-Experts.} Non-expert users achieved an average of 01:22 minutes using the data table for completing the second task. By using the software, they achieved an average of 01:34 minutes. The lowest time by using the data able during the second task was recorded 22 seconds. Using the software, the lowest recorded time was 58 seconds. The highest time achieved by using the data table was 02:55 minutes. By using the software, users achieved the highest time of 02:39 minutes.

\paragraph{Experts.} Expert users achieved the second task in 25 seconds, respectively 54 seconds using the data table. They achieved the task in 59 seconds, respectively in 1:01 minutes, by using the software.

\subsubsection*{Results of Task 3: Improve a Process without Assistance}

\paragraph{Non-Experts.} During the third task, non-expert users achieved the minimum improvement of 15\% in an average time of 04:57 minutes. 

The status quo process totalled to 48.9 seconds, hence 7,35 seconds was the least a user had to save by creating a new variation of the process. The average score achieved by the non-expert users in this task was of 29.73 seconds, while still leaving the process semantic intact. This maps to an average improvement of 41\% in comparison to the process the users were given. The least improvement achieved by non-experts was up to 41.43 seconds. This improvement was over the 15\% improvement margin, therefore each evaluatee completed the task successfully.

\paragraph{Experts.} The two evaluated experts achieved a shortened process from 48.9 seconds to 16.7, respectively to 22.38 seconds. Therefore, both completed the task successfully. They achieved the minimum improvement of 15\% in an average time of 04:32 minutes.

\subsubsection*{Results of the System Usability Scale Questionnaire}

In order to achieve an system usability scale score, the scoring method described in \cite{brooke1996sus} was used. This methods provides a grade from A to F for rating the system under test, with A being the best and F being the worst possible grade.

\paragraph{Non-Experts.} Non-expert users graded the software to C on the system usability scale score, averaging to 70.5 points. The worst system usability scale score given was of 57.5 points. The best available score was 92.6 points.
\paragraph{Experts.} Expert users graded the software to a C and a D on the system usability test scale. For the grade C, the expert users rating achieved a system usability scale score of 70 points. For the D rating, the user totalled to 67.7 points.