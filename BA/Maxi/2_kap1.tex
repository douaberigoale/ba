\section{Current State Analysis}\label{analysis}
The current state of research will be analysed during this section. The section will also contain
a brief analysis of existing software solutions. Furthermore, this section will look into the visual analytics field and its some of its methods.

\subsection{Current State}

Usability and user experience are very important components of any software or website in any field. Although task analysis and process breakdown ,in particular using different models, have been around since the dawn of computer user interfaces, this subject is still being currently researched and optimised. Alone for the keystroke level model, there are over 60 search results available for the first eight months of 2019 on Google Scholar\footnote{Google Scholar Website. https://scholar.google.de. Last accessed August 2019.}.

Nowadays, more research is focused on touch-based mobile user interfaces and how these interfaces can be quantitatively analysed. Task analysis plays a very important role in this research field. Multiple extensions to GOMS and the Keystroke Level Model for touch devices are still being released, such as the ones proposed by \cite{setthawong2019updated}.

 Another newly available paper has shown that there are significant relationships between usability, user satisfaction and continuance intention. Hence, usability and how processes are designed on a website are important aspects in how satisfied users are, thus also positively influencing the possibility they buy something or return again \cite{yassierli2019importance}.
 
 Optimal user processes and usability are not only important for acquiring new and returning customers. Usability also plays a great role in fields where decisions have to be made as optimally and quickly as possible. Besides having to provide intuitive and short processes, usability also has an influence on how users react if the system is erroneous. \cite{brauner2019happens} have shown that users who rated the usability of a system higher were also outperforming other users. They also concluded that errors affected the usability rating of the software.

Another type of ongoing research regarding the keystroke level model is combining it with machine learning approaches in order to automate its usage. There are some current research efforts to automate keystroke level modelling using reinforcement learning, a machine learning method. \cite{leino2019rl} defined the keystroke level model as an Markov decision process. The authors concluded, that by applying the keystroke level model operators on a Markov decision process and using reinforcement learning, they were able to achieve plausible process sequences. 

Despite being a field which is continuously being used, researched and improved, there are few software solutions which provide support to users in analysing processes, in particular in working with the keystroke level model.

\subsection{Software Solutions}

Some available software solutions for modelling processes using the keystroke level model or similar models will be discussed during this subsection. The options are mainly text-based in modelling with the predictive operator models. Although comparing and improving tasks is a very important component in task analysis and these models provide the base to do so, most of the solutions presented have limited capabilities in doing so. 

\subsubsection*{Data Table}

A very common way to use any type of operator based process quantification method is by creating a data table. This data table usually has some operations which provide calculations in order to ease the workload of the user. The simplest data table can be populated with the amount of operator usage for each operator available in the used model. It provides users with a cumulated process cost, based on multiplying each operator cost with the times it is required during the process and afterwards creating a sum of these costs. The sum is the final process cost. 

Table \ref{tab:datatablesolution} shows an example data table, which can be extended for any operator model. This data table contains only raw data and can be visualised, but no visualisation methods are available by default.

% Please add the following required packages to your document preamble:
% \usepackage{booktabs}
\begin{table}[]
\centering
\caption{Generalized data table method for working with operator models in order to provide the user with a total cost for the process in accordance to the number of times each operator is being used.}
\label{tab:datatablesolution}
\begin{tabular}{@{}lll@{}}
\toprule
\textbf{Operator} & \textbf{Cost} & \textbf{Times Used} \\ \midrule
O1 & 1 & 2 \\
O2 & 2 & 1 \\
 &  &  \\
\textbf{Total Cost} &  & 4 \\ \bottomrule
\end{tabular}
\end{table}

\subsubsection*{Keystroke Level Model Form Analyzer (KLM-FA)}
Another solution is the Keystroke Level Model Form Analyzer (KLM-FA), developed by \cite{Katsanos2013}. It allows the user to model processes by using any online form and recording their actions. This tool is different of other similar tools by focusing specifically on web form interaction. The reason for doing this is to minimise the needed effort during evaluation. Moreover, one further goal of this software is allow users to learn the model by being able to trace their steps back. 

This software provides however no support in comparing process variations.

\subsubsection*{Cogulator}
Cogulator is an open-source software solution which markets itself as "a simple human performance calculator (...) for estimating task time and difficulty.". It provides multiple GOMS-family model metrics to model processes. This software focuses most on the mental workload a user has while completing a task, as well as other types of sensory workloads. It provides support for live process modelling and live mobile device process modelling as well. 

Visualisation is present in this software, since it models processes by switching lanes on a canvas. Each lane depicts some kind of user workload.

\begin{figure}[H]
  \centering
    \includegraphics[width=0.7\textwidth]{abb/klmfa.png}
    \caption[Official screenshot of the Keystroke Level Model Form Analyzer (KLM-FA), a software solution which aims to providing support to users which wish to evaluate web form filling task.]{Official screenshot of the Keystroke Level Model Form Analyzer (KLM-FA), a software solution which aims to providing support to users which wish to evaluate web form filling tasks\footnotemark.}
      \label{fig:klmfa}
\end{figure}
\footnotetext{Keystroke Level Model Form Analyzer Official Website. \url{http://klmformanalyzer.weebly.com/} Last accessed August 2019.} 

\begin{figure}[H]
  \centering
    \includegraphics[width=0.7\textwidth]{abb/cogulator.png}
    \caption[Official screenshot of the Cogulator. This software aims to assist users in estimating task time and difficulty.]{Official screenshot of the Cogulator. This software aims to assist users in estimating task time and difficulty\footnotemark.}
      \label{fig:klmfa}
\end{figure}
\footnotetext{Cogulator Official Website. \url{http://cogulator.io/} Last accessed August 2019.}

\begin{figure}[H] 
  \centering
    \includegraphics[width=0.9\textwidth]{abb/cogtool.png}
      \caption{Official screenshot of the CogTool, a software which can be used to prototype UI and which automatically evaluates the design using a model such as the Keystroke Level Model \cite[Figure 1]{john2010cogtool}.}
      \label{fig:cogtool}
\end{figure}

\subsubsection*{CogTool}
CogTool was developed by \cite{john2010cogtool} and is a UI prototyping tool at its core. Besides allowing users to design their desired interfaces, CogTool also provides an automatic evaluation of the design by using a predictive operator model such as the Keystroke Level Model. CogTool provides support to comparing multiple variations of the same process. This tool is very design oriented and aims to automatise the evaluation of task analysis.

\subsection{Visual Analytics}
Visual analytics is an interdisciplinary field which allows users to interactively explore graphically exposed data \cite{keim2008visual}. Due to increasing amounts of raw data which needs to be analysed, the problem of information overload poses a threat to appropriately doing so. The risks include focusing on data which is irrelevant for the task, as well as processing or presenting it in an inappropriate way for users.

In order to make decisions using the information provided by big data sets, the information has to be synthesised. Based on the synthesised data, users can detect expected and unexpected information, as well as use the visualizations to easier and better understand the information and make appropriate decisions regarding the task at hand.  

Due to human perception, visualisations need to be carefully designed, as not to confuse users or overwhelm them with too much or inappropriate information for the task at hand. Since visual analytics is discipline driven, the advantages and opportunities of different approaches must be identified beforehand. 

Although being use case driven, visual analytics typically combines knowledge and approaches from different disciplines in order to generally achieve better understanding of the data and the workflow between users and machines \cite{keim2008visual}. Figure \ref{fig:visbothsides} shows how machine and human scientific disciplines work together in this field. On the one hand, there are machine scientific fields like Graphics and Rendering or Compression and Filter, which are applied on the data itself. On the other hand, knowledge regarding how human users perceive and process information, such as Human Cognition or Information Design, allow visual analytics to present information in such a way, that humans can better work with it.


\begin{figure}[!b] 
  \centering
    \includegraphics[width=1\textwidth]{abb/visbothsides.png}
      \caption{Depiction of the integration of different field areas and disciplines in Visual Analytics. \cite[Figure 2]{keim2008visual}}
      \label{fig:visbothsides}
\end{figure}

Besides challenges regarding human perception and cognition, technical challenges arise as well. Huge amounts of data must be further abstracted or parts of them disregarded in order to display them in visualizations. However, further abstracting or disregarding data might remove certain important patterns or general information, thus rendering the visualization useless in some cases.

\subsubsection*{Interactive Visual Analysis}

The Interactive Visual Analysis methodology assumes that showing huge amounts of complex data at once is impossible, thus needing user interaction to navigate through the data and extract relevant information for the task at hand \cite{kehrer2012visualization}. This field is emphasizing on the interaction component and provides some general components for a large area of use cases.

Some of the components are listed below. They are present in \cite{oeltze2012interactive}. 

\paragraph{Show \& Brush} Some kind of diagram of two dependent variables is being shown. After brushing an interval of data, another visualisation method focuses on the brushed data, displaying it in a different way.  

Show \& Brush allows different interactive visual analysis patterns, such as local investigation, feature localization and multivariate analysis.

\paragraph{Coordinated Multiple Views} Users can use the coordinated multiple views approach to observe one dataset in multiple view, such as different diagram types. By brushing or altering data, all views change accordingly and provide different point of views.

\paragraph{Focus+Context} Although being shown all available data, users can choose some dataset interval and focus on it by appropriate visual cues. Some of these visual cues might be emphasizing the data in focus in some visual manner, either by modifying the visual properties of the data interval itself or its background.

\paragraph{Linked Views} Linking views in a visualisation provides users with multiple diagrams of the same data interval, while allowing them to be able to brush just in one view and consistently see the interval being brushed in the other views.

\paragraph{Degree of Interest} By brushing, users define a certain degree of interest for some part of the data. The visualisation can react binary and define the brushed part as relevant and all other data irrelevant. However, another possible approach is to define a fractional degree of interest. By defining the degree this way, the continuous nature is respected.   