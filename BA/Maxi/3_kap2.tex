\section{Concept}\label{concept}

This section describes the concept for implementing the software, as well as concept changes which took place during development and the reasons of change. The original mockups for the software will be discussed.

\subsection{Target Group}
The target users of this software are usability experts, process creators and process evaluators. In order to enable a many users to properly use the software, the user actions are kept as simple as possible and only require a keyboard and a mouse.

\subsection{Target System} 
The software is designed to be web-based and accessible using any modern desktop browser. This supports the conceptual decision to enable heterogeneous categories of users to use the software, by enabling any person with a personal computer and an internet connection to use this software.

\subsection{Feature List}

Multiple features are present in the software and ensure simpler modelling using the keystroke level model, as well as process modelling and optimization support.

Firstly, processes are being modelled visually using nodes and edges. The interaction with nodes is intuitive and simple. These actions include creating, updating, deleting, linking, unlinking and renaming nodes. In order to improve working with the keystroke level model, predefined and quantized nodes are available. Nodes have keystroke level model types and users are able to view and change these types at any time.

Secondly, users are assisted using charts in order to acquire further information during process modeling and comparison. The charts display information about the whole process. Both the scatter plot and the cumulative histogram used for this purpose contain additional, real-time displayed information relating to either all graphs present on the canvas or a selected graph.

Lastly, users are assisted by a review function. This functionality displays and exports only the information related to the graph comparison. Due to the information provided, this tool serves as decision support.  

\subsection{Workflow} \label{workflow}
The ideal workflow for this software consists of four steps. 

The first steps involves modelling an already implemented process using the provided keystroke level model node types. This enables to user to have a keystroke level model quantified status quo process inside the software, analysing it, and create multiple abbreviations. 

The second step of the  consists of analysing the graph using the visual analytics tool in the sidebar. Using this tool, the user may acquire new knowledge about the flow of cost during the process, as well as of parts of the process, which might be improved later on. 

During the third step of the workflow, a user will create copies of the already modelled process and create new ways of completing the process based on the status quo. Moreover, users may choose to create completely new processes. 

The last step involves comparing the modelled processes in the software. This step can be achieved using the aggregated visualization in the sidebar, as well as using the review dialogue available via the upper menu button. The review dialogue shows information regarding the improvements in relation to the status quo.

\subsection{User Interaction}
The main user interaction is via mouse left or right clicks on the canvas, nodes, and menu bar buttons using dropdown menus or the sidebar.

In order to provide support for power users, common key shortcuts have been made available in the software. They can be reviewed in Table \ref{tab:shortcuts}.


% Please add the following required packages to your document preamble:
% \usepackage{booktabs}
\begin{table}[]
\centering
\caption{Key shortcuts available for power users in the software.}
\label{tab:shortcuts}
\begin{tabular}{@{}ll@{}}
\toprule
\textbf{Key Shortcut} & \textbf{Action triggered} \\ \midrule
Ctrl + C & Copy selection \\
Ctrl + V & Paste selection \\
Ctrl + X & Cut selection \\
Ctrl + P & Open Print Dialogue \\
Ctrl +Mousewheel & Zoom in or out on canvas \\
Ctrl + Z & Undo \\
Ctrl + Y & Redo \\
Ctrl + D & Duplicate \\ \bottomrule
\end{tabular}
\end{table}

\subsection{Architecture}

The Model-View-Controller, a software architecture pattern, was the main source of inspiration for the architecture concept of the software. It separates the application into three different components, which communicate with each other on runtime. The model component is either a data-access layer or a domain model used to depict some kind of entity. The second component, the view, is the output the user sees on screen and with which the user can interact. The last component, the controller, processes user inputs and reacts by either changing the view or data from the view model \cite{mvc2001}.

 Due to the base of the application, it was clear from the beginning that a strong separation between components was not recommended or easily done. However, conceptually, the keystroke level model classes had to be clearly separated from any other type of model, since it is the main data model available. Other models could be more integrated with the controller and the view.

\subsection{GUI}
The graphical user interface of the software consists of the canvas, the upper menu bar and the sidebar. 

The mockup shown in Figure \ref{fig:mockup} displays the user interface which was created during the concept phase. The mockup shown in Figure \ref{fig:reviews} shows the final concept for the review. They were drawn using Pencil\footnote{Pencil Official Website. \url{https://pencil.evolus.vn/} Last accessed August 2019.}, an open-source prototyping software for graphical user interfaces.

\subsubsection*{Changes}
During further conceptualising while implementing the software, the interface suffered some changes regarding functionality, arrangement and user interaction. 

Firstly, due to the heightened complexity of interaction and implementation, displaying multiple tabs for multiple canvases was abandoned in favour of encouraging modelling different variations of the same process on the same canvas. This decision was met due to a break in interaction in the workflow regarding needing users to constantly change between tabs to compare charts and their visualisations, as well as creating mental separation from the review. 

Secondly, since multiple canvases were not available during further concept phases any more, the breadcrumbs below the menu bar were deemed useless.

Thirdly, by displaying all variations of the same process on the same canvas, some adjustments regarding the chart in the sidebar were needed. 

Lastly, more information regarding the keystroke level model was included on the canvas and on the sidebar. This decision was made in order to allow users to see important information, such as the specific keystroke level model operator node types drawn in the graph on the canvas, at a glance. Furthermore, some kind of legend for this information was deemed necessary. The legend was positioned on the sidebar.

All previously mentioned changes in concept are being included in the further descriptions about the different parts of the graphical user interface of the software. All choices made during further conceptual phases or the implementation cleaned up the user interface, allowing it to be less cluttered, while also providing more useful information to the user.

\begin{figure}[!h] 
  \centering
    \includegraphics[width=1\textwidth]{abb/mockup.png}
      \caption{Mockup for the general workspace available in the software, designed in the first concept phase. It shows the canvas, menus and sidebar, as well as their placement.}
      \label{fig:mockup}
\end{figure}

\begin{figure}[!h] 
  \centering
    \includegraphics[width=0.7\textwidth]{abb/reports.png}
      \caption{Mockup for the review modal dialogue, which shows how the user should be able to review and print the modelled process variations.}
      \label{fig:reviews}
\end{figure}

\subsubsection*{Canvas} \label{canvas}
The canvas is the main working area, where the user can model processes visually using nodes and edges. The model is kept as simple as possible because its goal is to convey information. To do so, the nodes and edges are flat and without shadows or reflections \cite{foley1990computer}. In order to convey information regarding the keystroke level model at a glance, the operator keys, as displayed in Figure \ref{fig:klmoperators}, were chosen. These keys are displayed beside each node.

The software allows for multiple processes or process variations to be modelled on the same canvas. Due to its importance and spatial needs, the canvas takes up most of the screen space in the software. The user can freely move on the canvas and nodes may be freely placed on the available working area. Moreover, the canvas is infinite and extends to the right and down when the current dimensions are not sufficient or when the user drags a node out of the canvas. Users should not be allowed to drag nodes to the top or left and achieve a canvas extension. Infinite canvases are nowadays present in multiple software with different usages. They allow an expansion of the available story space, thus creating context and sequentiality \cite{salter2013spirals}.

Besides drag and drop, click and double click actions are available on the canvas, users can open different context menus by right clicking either on nodes or the canvas itself. These context menus allow users to quickly perform basic actions, such as creating root nodes on the canvas or subsequent nodes, as well as deleting nodes.

\subsubsection*{Menu bar}
The menu bar is commonly a primary component of the software and provides common application commands \cite{Galitz2007}. The upper menu bar has multiple dropdown menus. Most of the actions available in the menu bar affect or work with the whole canvas.

The menu bar is divided in three submenus. Each submenu opens as a dropdown element, where users can choose their desired action. 

The first one, File, contains four possible actions. Users can either open saved graphs or save the current canvas on the local file system. Furthermore, users can use the "Save as..." functionality to save the current canvas with a custom name. The fourth possible action allows users to print the current canvas either as a poster or on a custom number of pages, while also setting the page scale. Users can preview the canvas and save it in the Scalable Vector Graphics format or print it right away. If choosing print, users will be confronted with the default print dialogue on their computer.

The second submenu, Edit, contains actions which affect all or the selected nodes on the canvas. Users can either undo or redo previous actions. Common actions, such as cutting, copying, pasting or deleting selected nodes can be selected. Duplicating the current selection is also available in this submenu. Accessing the properties of the selected node and editing their data is available under the "Edit Data..." button. Users can also opt to select all vertices, all edges, everything on the canvas or deselect all graph elements. Locking and unlocking the canvas is another available action, which enables users to either make the elements on the canvas unresponsive to clicking or typing, as well as making them react normally again.

The third submenu, Review, holds the available actions regarding the review which can be generated from the data on the canvas. Users can either display the review in an overlay modal dialogue or print it directly. By choosing to display the review, users can asses the data and either close or print it.

\subsubsection*{Sidebar} \label{sidebar}
The sidebar assists the user by proving three types of functionalities and user actions, as well as dynamically updated information in sync with the canvas and a permanently displayed legend.

Firstly, it provides the user with information about all modelled processes or one particular process, depending on the users selection in the visual analytics chart dropdown. The sidebar contains both the cumulative and the action charts for the selected process or every process, depending on the user setting. If the user selects viewing the charts for all processes, two separate charts for the sequential action costs,respectively for the cumulated costs are displayed. These charts contain data available in every modelled process on the canvas. The user may choose to display the charts only for one process available on the canvas. This is done by using a dropdown element which shows all currently available roots on the canvas. By selecting one root, users are shown both the action and the cumulative chart in one combined diagram only for the selected process. Since it displays the charts, the sidebar provides information about the increase in cost of the process or processes, as well as displaying information about how the modelled process action costs differ during the process.

Secondly, the user selects the status quo process present on the canvas using a dropdown interaction element on the sidebar. This choice affects how the review is generated afterwards and reminds the user at any time which of the modelled process is the status quo process since the selection is permanently displayed on the sidebar.

Thirdly, while having selected a node on the canvas, the user may change the type of the KLM operator for it using the corresponding field on the sidebar. Furthermore, if the user selects the custom node type available in the software, then an additional field appears on the sidebar, where the user may enter a custom cost for the node. Similarly, the user may edit the label of the selected node using the appropriate field on the sidebar.

Lastly, a legend of the keystroke level model operator keys is always visible on the sidebar. This works together with the operator key displayed by each node on the canvas in order to tell users at a glance what node types the modelled process use.
\newpage
\subsubsection*{Review Dialogue} \label{review}

The review dialogue is the final step in the proposed workflow. 

By opening the review modal dialogue window or printing it directly, users can view a diagram for all charts. This diagram can also displayed on the sidebar. 

Besides seeing the visualisation, users can see a tables in the review. The table compares all graphs available on the canvas to the selected status quo. It shows the total cost for each of the modelled processes, as well as the percentage of difference to the status quo.

Users can choose to print the review by clicking the respective button on the review modal dialogue panel.

\subsection{Visual Analytics}
In order to further provide meaningful but abstracted and synthesised information, multiple visual visualisations and means of interaction were considered during the conceptualization of the software. 

The software deals exclusively with data which is best presented to the user in accordance to its sequence and cost variable, when represented in a visualization. The cost is set by the keystroke level model operators and assigned to each node. Since the sequentiality of the data is of importance due to the linearity of processes, only relevant visualization techniques in regard to this aspect were considered. 

The cost is being treated as a property of the node, meaning it should be available to the user at a glance by means of visualization, but not by displaying numbers directly. In order to achieve this, the available visualizations should implement some kind of detail on demand component. The details visualization is conceptually designed to show the cost and label of each node.

\subsubsection*{Visualisations}

\begin{figure}[H] 
  \centering
    \includegraphics[width=0.9\textwidth]{abb/scatter.png}
      \caption{Diagram depicting a scatter plot. This is a type of linear, static and univariate diagram which uses a  well-known Cartesian  coordinate system \cite[Figure 7.1]{aigner2007visual}.}
      \label{fig:scatter}
\end{figure}

\paragraph{Scatter Plot} Scatter or point plots are linear, static and univariate diagrams and can display time series data in a Cartesian coordinate system \cite{harris2000information}. This diagram type can be used to emphasize individual data points and uses a commonly known scale, which can be useful for interpreting it \cite{aigner2007visual}. Figure \ref{fig:scatter} shows a scatter plot.

Due to the adaptability of this diagram and the fact that most users can read the information out of it easily, it was chosen to be implemented.

\begin{figure}[H] 
  \centering
    \includegraphics[width=0.9\textwidth]{abb/line.png}
      \caption{A line plot diagram, which is commonly used to display time related data by using lines or different type of curves. It is linear, static and univariate \cite[Figure 7.2]{aigner2007visual}.}
      \label{fig:line}
\end{figure}

\paragraph{Line Plot} Line plots are linear, static and univariate diagrams and commonly used to display time related data \cite{harris2000information}. Different type of curves can be used in this diagram, depending on the use case. Missing data is considered an issue whilst using this visualization type because it forces either approximating the missing data points or connecting nonsequential data points. Both approaches might lead to false conclusions \cite{aigner2007visual}. Figure \ref{fig:line} shows a line plot.

Since a process modelled in the keystroke level model cannot have any missing data points or nonsequential actions on an interface, this type of diagram was deemed useful to depict some kind of development of the cost over time.

\begin{figure}[H] 
  \centering
    \includegraphics[width=0.9\textwidth]{abb/bar.png}
      \caption{Diagram showing a bar graph, which uses bars to display values and can also be used for time related data \cite[Figure 7.3]{aigner2007visual}.}
\label{fig:bar}
\end{figure}

\paragraph{Bar Graph} Bar graphs are linear, static and univariate visualisations which can be used to depict time data values by using the bars height \cite{harris2000information}. They allow users to focus on individual values and overall development \cite{aigner2007visual}. Figure \ref{fig:bar} shows a bar graph.

Since bar graphs could confuse users by focusing on the differences of the neighboured costs in opposition to the sequential development of the cost during the process, they were deemed as less suited for visualising the action chart. However, bar graphs might be useful for displaying the cumulative chart.

\begin{figure}[H] 
  \centering
    \includegraphics[width=0.9\textwidth]{abb/horizon.png}
      \caption{A horizon graph, used to show and compare time-dependent data. They use less vertical space and mirror the values at the zero line \cite[Figure 7.6]{aigner2007visual}.}
\label{fig:horizon}
\end{figure}

\paragraph{Horizon Graph} Horizon graphs are linear, static and univariate diagrams which can be used depict and compare multiple time-dependent variables \cite{reijner2008development}. They use less vertical space and increase data density on the screen. Because of the horizontal mirroring of values at the zero line, the visualisation technique is colour coded \cite{aigner2007visual}. Figure \ref{fig:horizon} shows a horizon graph.

This visualisation type is not recommended either for the action chart, nor for the cumulative chart. The reason for this is that no values in the software can be negative, due to the implemented keystroke level model.

\begin{figure}[H] 
  \centering
    \includegraphics[width=0.9\textwidth]{abb/braided.png}
      \caption{A braided graph, which deals with linear, static and multivariate data. It combines two or more variables by using transparency or braiding \cite[Figure 7.45]{aigner2007visual}.}
\label{fig:braided}
\end{figure}

\paragraph{Braided Graph} Braided graphs are linear, static and multivariate visualisation techniques which combine two or more variables by using transparency or braiding \cite{javed2010graphical}. Although they allow users to be faster when searching for local maxima, they cause decreased correctness and increased task completion time in studies which dealt with high numbers of time series \cite{aigner2007visual}. Figure \ref{fig:braided} shows a braided graph.

Since the action chart and the cumulative chart should be most useful to users when the process they want to improve contains many actions, thus containing many nodes and costs, a braided chart is not recommended for any of them. Decreased correctness and increased task completion time are both the opposite of what the software tries to achieve by providing charts to users.

\begin{figure}[H] 
  \centering
    \includegraphics[width=0.9\textwidth]{abb/silh.png}
      \caption{Silhouette graph and a circular silhouette graph, both dealing with static and univariate data. The silhouette graph can show linear data and the circular silhouette graph can also depict cyclic data. Both can be used to visualize time developments \cite[Figure 7.24]{aigner2007visual}.}
\label{fig:silh}
\end{figure}

\paragraph{Silhouette Graph/Circular Silhouette Graph} Silhouette graphs are linear or cyclic, static and univariate diagrams which can be used to represent time data in filled areas \cite{harris2000information}. By using this visualisation type, multiple data sets can be stacked upon each other. Circular silhouette graphs use concentric circles axes to represent the same visualisation type \cite{aigner2007visual}. Figure \ref{fig:silh} shows both a silhouette graph and a circular silhouette graph.

Silhouette graphs or their circular counterparts could be useful in depicting information about all the variations of the processes on a canvas. They were, however, not included in the concept in order not to confuse the user with an additional chart type.

\subsubsection*{Interaction Techniques}

Users can profit more from any visualisation technique by having means of interacting with it. Being able to focus on particular data points or changing the displayed information can provide users with more insight regarding the visualised data. Therefore, users can attempt to answer a larger range of questions by changing their point of view.

Users also have different and specific reasons to interact with visualisations, as described by \cite{yi2007toward}. Some of them are relevant for the use case in this software. Those are selected from \cite{yi2007toward} and enumerated further.

\paragraph{Select} Users want to be able to visually encode or highlight something, if it is deemed as interesting to them. By interacting with the diagram, users should see some feedback on the canvas.

\paragraph{Encode} Just one type of diagram for displaying the data is probably not enough to gain a lot of insight for the most users. The processes should have at least two different visualisations generated from them.

\paragraph{Abstract/Elaborate} Users model processes on canvases into graphs, however they should be able to see some type of diagrams which provides, at least at first, only the most relevant information. Since the graph does not have an explicit view of the cost, the most relevant information to be displayed in the diagrams are the node costs.

\paragraph{Filter} Multiple charts available on the same diagram, either displaying the data from multiple processes in order to compare it or displaying the data from the same process in different ways, can be helpful in gaining insight. However, users should be able to use some type of instant filters on any diagram, in order to quickly observe just some part of the data at any time. 