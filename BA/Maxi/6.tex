\section{Discussion}\label{discussion}

The two evaluated user groups will be treated differently during the discussion. Since the non-expert user group is significantly larger and much more heterogeneous than the expert group, some emphasis will be on the data averages collected. While regarding the expert group, the emphasis will be set on the qualitative analysis. This is possible due to the fact that both user groups were asked to think aloud during task completion and most users complied. Furthermore, after completing the evaluation, some non-expert users and both experts expressed thoughts regarding the software. They will also be discussed here.\\

\subsection{Non-Expert Group}

During the evaluation, most non-expert users had difficulties understanding the complete purpose of the software and the keystroke level model, until reaching the third task. This was most likely caused by the lack of field experience. Some non-expert users expressed their satisfaction in having a visual tool at their disposal, in comparison to the data table, when they reached the third task. 

Some non-experts were overwhelmed with having multiple visualisation methods at hand, therefore only concentrating on the graph structure on the canvas during the second and third task. In contrast, one non-expert user solved the third task by extensively using the action chart. 

One non-expert user recommended using colour in order to differentiate the node types on the canvas. This idea was disconsidered at the beginning of the conceptual phase due to the large amount of node types available.

Since all non-expert users were able to successfully accomplish all tasks with the minimal information regarding the keystroke level model and usability in general, the concept goal of providing a simple solution to using this operator model to a large audience has been met. Furthermore, some non-expert users recognized themselves that the third task would be impossible to complete just by using the data table method, they had however no issue with completing this task successfully using the software. 

Despite potentially having to perform more clicks, users achieved a better time average during the first task by using the software. However, the same user group achieved a slightly worse time during the second task, in comparison to using the data table. This suggests that users might have no performance differences in modelling with the data table or the software. They might, however, require adjustments to the software behaviour when modelling a variation of the process.

The C grade given on average by the non-expert users to the software, according to the system usability scale questionnaire, in combination with the improvements achieved during the third task, an average of 41\% improvement over the status quo process, suggest that the software can be used to work with the keystroke level model, but still has room for improvements regarding its usability.

In conclusion, this evaluation has shown that non-expert users can handle using the software well, despite not having much field knowledge. However, the software can and should be improved in regard to its usability.

\subsection{Experts}

Expert users performed well in all tasks. Their evaluation task duration times were similar to the times of the non-expert user group. 

The first expert user found it useful to be able to create nodes by right clicking on existing nodes. They saw not having to connect nodes themselves during the modelling as efficient. They were also interested about the review modal dialogue as a means to have the whole project information at a glance and easily printable.

Both experts found the charts available in the sidebar useful, however not for short processes. One expert expressed their concern with non-experts being overwhelmed because the charts are displayed permanently and not necessary useful in easy use cases. One expert found the legend for the keystroke level model very useful in general.

Both experts also liked that the software was built as a website and could be accessible anywhere, as well as being able to start right away using it. One expert found the present save and load functions necessary for bigger projects.

The rating of C, respectively D on the system usability scale from the experts is also very similar to the one from the non-expert group.

In conclusion, both experts found some likeable features while using the software. Their scores during the evaluation were similar to those of the non-expert groups. However, no proper averages can be built out of this user group due to the small number of participants.

\newpage

\section{Future Work} \label{Future Work}
There are multiple ways of improving the usage of user experience of the software and allowing users to gain more by using it.
 
\subsection*{Handling Process Split-Ups}
In order to better portray measurements related to processes using visual analytics, the charts displayed in the sidebar and in the review panel need to be able to handle graph split-ups in modelled processes. This functionality might be achieved via different approaches. Each option has compromises and disadvantages.

A possible approach to implementing this feature using the present data structure is reading and processing the children of each graph node. While reading them, the algorithm must either duplicate each parent node in order to generate separate chart data. Another method is treating the children of each node as roots, therefore as separate graphs. This approach has been proven problematic using the current chart library and could prove being highly inefficient if the modelled processes are large and contain main forks.

Another approach would be to navigate the modelled process forwards until obtaining information regarding the leaves of the graph or requesting all nodes without any children from the graph. After gathering this information, the nodes can be navigated backwards recursively through their parents, until the root is reached. By recursively navigating and storing the nodes of the different leaves, the multiple sub-processes are discovered and may be then processed into data points for the charts. This method might prove problematic with complicated forks and joins in the process and might generate huge amounts of information because many paths have to be backtracked.

A third option in dealing with split-ups is based on the assumption, that the process user will take the cheapest sub-process path to successfully complete the process, in disregard of how many other paths exist. Therefore, only the cheapest path from root to completion is relevant when comparing the status quo process with newly modelled process alternatives. To display this information, a similar approach to the second idea might be used. However, instead of storing multiple sub-graphs, only the path with the minimal cost will be stored for comparison. It will be replaced with another path if its cost exceeds the cost of the previously stored path. The shortest path will be displayed afterwards.

\subsection*{Further Visualisation Support}

The implemented software contains two different type of visualisations which are both appropriate for their specific use case. They were adapted to suit each use case specifically and assist users as best as possible in analysing and comparing processes modelled on the canvases. However, more visualisation techniques might be suited for visualising processes. 

In general, more visualisation techniques should be tested on user response either inside the software or just in combination with the keystroke level model or a similar operator based model. Some possibly appropriate methods were considered during the conceptual phase of this thesis. The most appropriate methods which should be tested on user response are the bar graph and the silhouette graph. Bar graphs might be useful in displaying the cumulative chart. The silhouette graph and its extension, the circular silhouette graph, might be useful for both the action and the cumulative graph, due to its general support towards comparing sets of data. 

Moreover, users might profit from being able to visualise the action or cumulated chart data by using multiple diagram types. In this way, users can set up their preferred method for visualising the processes they model. This might improve the subjective performance in working with the software. 

Finally, multiple visualisation types should be evaluated in combination with the keystroke level model or other similar models in different fields. Using different visualisation types for different application fields could prove beneficial in creating a better understanding of which visualisation needs different use cases have. 

\newpage

\section{Software Potential}
While being explicitly implemented to use the keystroke level model, the software may allow other models which are quantized. Since the software concept and implementation can be easily separated from keystroke level model by replacing it with another model type or allowing users to feed in their custom models, the software may be able to model any kind of sequential and quantized data.

\subsection{Concept}
This idea will be further discussed in a potential concept.
\subsection*{Data}
In order to use this software with other types of operators, the user must provide a type of file format containing labels and costs for the new operators, as well as a unique unit of measurement. Each data point has to have a cost, while multiple data points are allowed to have the same cost. The user should not be allowed to provide labels without costs, therefore needing a check of the parsed data. Enabling or disabling the custom node type, which can be used to create nodes using user set costs, should be controlled using a setting in the software or a property in the data model. This data may be provided in a XML format\footnote{XML Technology. \url{https://www.w3.org/standards/xml/}
Last accessed August 2019.}.

The PROPERTIES object in Code Snippet \ref{lst:xml} should contain all relevant settings and general properties across the objects, for example, the availability of the customizable node type while using the loaded operators or the unit for the costs of the operators.

Each operator may then be defined separately. In the example XML data structure in Code Snippet \ref{lst:xml}, the software expects a label, a cost, a description of the operator and an icon path for the operator for each node object to be created.

\begin{minipage}{\linewidth}

\begin{lstlisting}[language=XML, label={lst:xml}, caption={XML data file structure example for feeding model operator data in the software.}]
<DATA>
	<PROPERTIES>
		<CUSTOMNODE>true</CUSTOMNODE>
		<UNIT>CM</UNIT>
	</PROPERTIES>
	<NODE>
		<LABEL>Label 1</LABEL>
		<COST>1</COST>
		<DESCRIPTION>This is Label 1 with cost 1.</DESCRIPTION>
		<ICON>ICON PATH ON SERVER</ICON>
	</NODE>
	<NODE>
		<LABEL>Label 2</LABEL>
		<COST>2</COST>
		<DESCRIPTION>This is Label 2 with cost 2.</DESCRIPTION>
		<ICON>ICON PATH ON SERVER</ICON>
	</NODE>
	<NODE>
		<LABEL>Label 3</LABEL>
		<COST>2</COST>
		<DESCRIPTION>This is Label 3 with cost 1.</DESCRIPTION>
		<ICON>ICON PATH ON SERVER</ICON>
	</NODE>
<\DATA>
\end{lstlisting}
\end{minipage}

\subsection*{User Interaction}
The user must be able to input the operator data file via upload or text box. If there is no default operator data, the user should be prompted via a permanent pop-up dialogue, that they must load a data model. The user may proceed in using the software after doing such. 

Depending on the type of data model loaded, a user should be able to choose if an increase or a decrease of total cost of the modelled process should be regarded as better or worse. This information may also be stored in the PROPERTIES object in the data model, if known beforehand.

\subsection{Implementation}
Multiple changes must be completed before the custom data model feature is possible to use and stable.

\subsection*{Changes} Since the current model is hardcoded inside the software, this part should be removed before starting the further development for this feature. All specific keystroke level model areas should be removed and the base data structure which describes nodes may be kept, however, should also be cleansed of any keystroke level model specificity. 
\subsection*{New Features} Multiple new features could be added to the software in order to provide a smooth and stable interaction with the software. 

A landing page for choosing, uploading or inputting the data model to use in the software is needed. The landing page should appear any time the user requests it to load a new data model, as well as at the start of the application. It should be explicit about needing to input a data model to the software, as well as about the means of doing so and possible errors, if they should be displayed. Such a embeddable tool was developed by The University of North Carolina and is available under the Apache License 2.0\footnote{UNC-Libraries/jquery.xmleditor Repository on GitHub. \url{https://github.com/UNC-Libraries/jquery.xmleditor} Last accessed August 2019.}.

As previously stated, the user needs to be able to upload new data models. At least one of these methods should be available. Inputting the data model would require a text input box available on the landing page, whilst uploading the file would require either an additional library or using a feature like the W3C File API\footnote{FileAPI - W3C Working Draft \url{https://www.w3.org/TR/FileAPI/} Last accessed 2019.}.

After uploading a file, the file must be parsed in order to feed the data model information into the software and afterwards use the operators. Assuming the data model is encoded in XML and structured as displayed in Code Snippet \ref{lst:xml}, the XML file or information must be processed. This may be done using the DOMParser\footnote{DOMParser - MDN web docs. \url{https://developer.mozilla.org/en-US/docs/Web/API/DOMParser} Last accessed August 2019.}. Since the XML data must be compliant of its standard\footnote{XML Technology. \url{https://www.w3.org/standards/xml/} Last accessed August 2019.}, parsing errors should be handled and fully displayed to the user, if existent, so that the user can fix their data model before trying again to feed it in. A small XML editing facilty may be added using a modal menu, where the user is able to tend to the caught syntax error and afterwards directly submit the XML data again.

After processing the operators inputted in via XML data model, a separate feature is needed to store the acquired data. An effective and efficient way to do this for would be to save the processed data in local storage of the browser\footnote{Local Storage. \url{https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage} Last accessed August 2019.} and use generated or hardcoded dictionary keys to read out and work with the data. Both writing and reading data out of the local storage in the browser is an available living standard and supported by all major browsers at the moment.