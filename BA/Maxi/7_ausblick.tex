\section{Implementation}\label{implementation}
The software is implemented as a web-based application, which is accessible via modern desktop browsers. In order to achieve ease-of-use and a productivity increase by using the keystroke level model, most interactions with the software are visual and intuitive.

\subsection{Programming Languages}
The software was developed using multiple programming languages.

\subsubsection*{JavaScript}
JavaScript is a dynamic language which supports multiple programming styles and is being used as a client-side programming language by over 90\% of all websites\footnote{Statistics regarding the usage of JavaScript. \url{https://w3techs.com/technologies/
details/cp-javascript/all/all} Last accessed August 2019}.

JavaScript is the main developing language used for the software, as well as the base for all libraries used. 

\subsubsection*{TypeScript}
TypeScript is a strict syntactical superset of JavaScript and allows developers to implement various concepts more easily. However, it must be transcompiled to actual JavaScript code. Some features of TypeScript include better object oriented programming paradigm support, as well as currently having better integrated IDEs available and tool refactoring support due to its strict syntax. The IDEs which are currently available with TypeScript support integrate code testing measures in order to provide the user with live feedback regarding code quality and errors.\\

This programming language is to define the keystroke level model logic required to assign KLM operators to nodes, as well as to compute the data for the scatter and cumulative histogram charts from the implemented keystroke level model operators. Since TypeScript requires a stricter syntax, as well as encouraging the explicit usage of types, it is very valuable in working with structures such as the keystroke level model operators, which may be treated as objects and profit from using classic inheritance and extending programming principles.\\

The following captions show the difference between using TypeScript or JavaScript during object oriented development. Both code snippets express the same functionality.

\begin{lstlisting}[language=JavaScript,label={lst:tscode},{caption=TypeScript enum code example.}]
export enum NodeTypeEnum {
    ButtonClickNode = 2,
    DropdownNode = 3,
    DatePickerNode = 4,
}
\end{lstlisting}

\begin{lstlisting}[language=JavaScript,label={lst:jscode},caption={JavaScript compiled code from TypeScript.}, breaklines=true]
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NodeTypeEnum;
(function (NodeTypeEnum) {
    NodeTypeEnum[NodeTypeEnum["ButtonClickNode"] = 2] 
    = "ButtonClickNode";
    NodeTypeEnum[NodeTypeEnum["DropdownNode"] = 3] 
    = "DropdownNode";
    NodeTypeEnum[NodeTypeEnum["DatePickerNode"] = 4] 
    = "DatePickerNode";
})(NodeTypeEnum = exports.NodeTypeEnum 
   || (exports.NodeTypeEnum = {}));
window.NodeTypeEnum = NodeTypeEnum;
\end{lstlisting}

While defining an enumeration object using TypeScript is straightforward and short, as depicted in Code Snippet \ref{lst:tscode}, the JavaScript counterpart is complicated, longer and more prone to logical or typing errors, as seen in Code Snippet \ref{lst:jscode}.


\subsection{Libraries}

Some libraries have been used in order to provide multiple functionalities to the software.

\subsubsection*{mxGraph}

mxGraph 4.0.0\footnote{mxGraph Official Website. \url{https://jgraph.github.io/mxgraph/} Last accessed
August 2019.} is the main library used for the graph functionality of the software. This JavaScript diagramming library provides interactive graph and chart features for major browsers. For this software, only the front end aspects of mxGraph are used and providing other functionality, such as downloading modelled graphs, was delegated to other libraries. mxGraph is licensed under the Apache License 2.0 and is used by many major companies for providing graph modelling components in their software.

GraphEditor\footnote{mxGraph Javascript Examples. \url{https://jgraph.github.io/mxgraph/javascript/index.html} Last accessed August 2019.} is the base of the software and build on mxGraph. It contains multiple extended functions which use mxGraph. It provides the GUI and import/ export functionalities, as well as graph drawing facilities. 

mxGraph itself was extended over time to provide support facilities in dealing with graph structures. Some helpers are edge linking, canvas setting definition, and grid snapping. 

A disadvantage of mxGraph is its software architecture. Since it is completely developed in JavaScript, there are few consistency guards regarding the hierarchy and inheritance between nodes. mxGraph is also conceptually designed to provide a visual diagramming component and not necessarily a base for developing complex software with strict rules for an operator based model. It does not lack support in providing custom attributes for nodes, but rather in using them. Developing extension modules in order to use saved attributes inside mxGraph can be daunting, since the attributes are saved as plain strings. Due to this, they might be involuntarily corrupted. Moreover, they must always be parsed into some kind of fixed type for using them with strict operators. This defeats the purpose of using a dynamically typed language and can generate unexpected errors.

\subsubsection*{Chart.js}

Chart.js 2.5.0\footnote{Chart.js Official Website. \url{https://www.chartjs.org/} Last accessed August 2019.} is an open source JavaScript chart drawing library available under the MIT license\footnote{MIT License. \url{https://opensource.org/licenses/MIT} Last accessed
August 2019.}. It provides multiple chart types, which can also be combined. Available chart types are line, bar, pie, scatter, radar charts and others. Due to its ease-of-use and customizability, Chart.js is frequently used to display interactive charts on websites and JavaScript applications. 

A disadvantage of using Chart.js is its inability to export files in the SVG format. 

\subsubsection*{download.js}

Download.js 4.2\footnote{download.js - Client-side file downloading using JS and
HTML5. \url{http://danml.com/download.html} Last accessed August 2019.} is a JavaScript library which provides client-side file downloading functionalities. The download functionality is highly customizable and can save any type of files with String, Blob or Typed Array content. 

A downside of using download.js is that the Internet Explorer 9 and below are not supported. Furthermore, any type of device which has no file systems cannot download data using download.js. Some devices without file systems include iOS powered devices and Wiis.

\subsubsection*{print.js}

Print.js 1.0.6.1\footnote{Print.js - A tiny javascript library to help printing from the
web. \url{https://printjs.crabbly.com/} Last accessed August 2019.}  is a small JavaScript library which provides the functionality of triggering file printing via a websites or other JavaScript applications. It uses the native print dialogue, where the user can select the destination of the file, as well as choosing other print related settings.

While using print.js, PDF files must be served under the same domain name as the location of triggering the print action, since the iframe technology it is based upon is limited by the Same Origin Policy in order to prevent cross-site scripting attacks.

\subsection{Other tools}
While developing the software, some other tools have played an instrumental part.

\subsubsection*{Webpack}

Webpack 4\footnote{Webpack Website. \url{https://webpack.js.org/} Last accessed August 2019.} is a module bundler and is mainly used to bundle JavaScript files. However, it may bundle other web front-end resource files and generate static assets out of them as well.  

Since TypeScript has to be transcompiled, webpack was used to build and bundle the present TypeScript files.

\subsubsection*{TSLint and ESLint}
Both linting tools, TSLint\footnote{TSLint Official Website. \url{https://palantir.github.io/tslint/} Last accessed August 2019.} and ESLint\footnote{ESLint Official Website. \url{https://eslint.org/} Last accessed August 2019.} are static code analysis tools which check TypeScript and JavaScript code, respectively, in terms of semantic and syntactic errors. While not having an active role in the developed software, both linters hugely improved the development process and the code quality, thus improved development performance, and subsequent code extendibility and maintenance. 