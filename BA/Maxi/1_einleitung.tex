\section{Introduction} \label{introduction}

\subsection{Terms}

Multiple terms will be used during this paper. These terms are clearly separated from each other by meaning and will be defined during this subsection.

\paragraph{Process.}A process is a string of atomic user actions on an interface which a user has to do in order to successfully complete a workflow. 

\paragraph{Graph.}A graph refers to the visually modelled process inside the implemented software. It contains nodes and edges. The graph is modelled on a canvas.

\paragraph{Canvas.}The canvas refers to the area on which users can create connected nodes.

\paragraph{Nodes.}Nodes are one user action, as described in the keystroke level model. Nodes might be broken into multiple nodes, as per composed keystroke level model operators. 

\paragraph{Edges.}Edges connect nodes and contain information about the order in the process actions. 

\paragraph{Chart.}Charts are visually encoded information regarding the graph. 

\paragraph{Cumulative Chart.}The cumulative chart is a cumulative histogram, which depicts how the process cumulates its cost over time. 

\paragraph{Action Chart.}The action chart shows the used nodes and their keystroke level model operator costs contained in the graph iteratively. 

\paragraph{Evaluation Terms.}The evaluation tasks are designed workflows which test users have to complete. They contain evaluation task steps. All tasks are part of the evaluation process, which has a clearly defined order.\\

To summarize, the software is conceptually designed such that a process can be modelled by using a graph on a canvas, analysed using a cumulative chart and an action chart. These relationships are depicted in Figure \ref{fig:terms}.

\begin{figure}[!h] 
  \centering
    \includegraphics[width=0.6\textwidth]{abb/Terms.png}
      \caption{Relationship diagram for the general terms used in this thesis.}
       \label{fig:terms}
\end{figure}

The evaluation process for the software contains multiple tasks which are defined through their evaluation task steps. The goal is to compare the software to another way of using keystroke level model. The design of the process is depicted in Figure \ref{fig:evalterms}.

\begin{figure}[!h] 
  \centering
    \includegraphics[width=0.6\textwidth]{abb/EvaluationTerms.png}
      \caption{Relationship diagram depicting the evaluation terms used during this thesis.}
      \label{fig:evalterms}
\end{figure}

\newpage

\subsection{Motivation}
Modern software interfaces allow users to do huge amounts of heterogeneous tasks. During interactions, users attempt to complete processes of different complexities. Unnecessarily complex or long processes tend to confuse or frustrate users, thus gravely denting their performance or making them drift away from one software solution to its alternatives. Usability evaluations and experts attempt to alleviate said frustrations by improving interfaces and developing processes which are intuitive and easy to use. Paying attention to the quality of use of software achieves the objective of usability \cite{Bevan1995a}. 

Usability experts which want to improve processes available on interfaces consider multiple ways of doing so. One effective way is to involve some kind of task analysis and process quantisation methods, in order to be able to model or compare processes. However, the ability of experts to model and compare user processes is hindered due to the few software solutions available. Furthermore, there are many ways to visualise and analyse time-related data and experts might profit from having the process data displayed in some other way, while modelling or comparing processes and their variations.

\newpage

\section{Task Analysis}
Task analysis, as described by \cite{kieras2003goms},  provides means to summarize, depict and evaluate what the user attempts to accomplish. It has a significant role in the development of requirements, as well as user interface design, interaction and evaluation. Since users attempt to successfully accomplish their tasks, the goal of a task analysis on a product is to ascertain what the user needs to accomplish and provide the functionality to do so. \\

Despite that task analysis is informal in its character, due to the heuristics and subjectivity, some formalized methods have been developed. 

Some methods, such as user behaviour observation or having users complete questionnaires, as well as thinking aloud or interviews, gather data about how the users respond to the task at hand.

After collecting the data, other methods have to be used by the analyst in order to decompose the task in simpler structures in order to analyse it, as well as decide on the needed level of detail in the abstracted process. The cost of task analysis rises with the level of detail which is examined.

High level task analysis takes account of the whole system in relation to the task. Analysts assert how the task is interwoven with the surrounding system. They also analyse how different isolated entities interact with each other and the task in order to complete it.\\

Task analysis also needs means of representing the analysed tasks. 
Procedural and other types of knowledge the user needs to be able to successfully accomplish the tasks have to be represented. Cognitive task analysis takes in account this information and how this knowledge is used, rather than the procedures involved.

Representing what the user has to do is a major form of task analysis. It focuses on the actual actions or activities, in contrast to cognitive task analysis. Experts focus on the sequence and timeline of operations in a process, as well as how the user interact during these operations in order to analyse how easily the task can be completed or possible issues and errors which might appear.

\subsection{Cognitive Psychology}

Cognitive Psychology studies mental processes and finds its usage in many heterogeneous areas, such as other branches of psychology. This branch of scientific psychology is concerned with how people comprehend and learn information, as well as how they solve problems. Due to developing and testing theories regarding human perception and processing, cognitive psychology is relevant to different types of interactions between humans and other mediums, therefore computer interfaces as well \cite{barnard1995contributions}.

Historically, Cognitive Psychology is one of the several disciplines which influenced the human-computer interaction research field.

\subsection{Human-Computer Interaction}

Human-computer interaction is a field of study which focuses on how interfaces between users and computers work and can be optimised, as well as design opportunities for better interaction. It encompasses and unites social and behavioural sciences, as well as computer and information technology \cite{shneiderman2010designing}.
Since the emergence of personal computers in the 1970s, huge amounts of users are able to access interfaces on computers. This continuous wave of new users, as well as constantly emerging technologies, created the need of better usability of devices and software. Nowadays, knowledge from the human-computer interaction field is of core importance in devices and software, deciding if users can easily and intuitively learn and use the products. Due to its importance, this field is in constant development. 

One study has shown, that users assign higher priority to navigation and interaction related aspects of interfaces, for example of websites \cite{calisir2010relative}.

\subsection{Human Processor Model}

The human processor model is a cognitive modelling method, which is used to calculate the time needed to complete a task \cite{card1986model}. The model draws analogies between how a computer processes and stores data to how users process the data using perceptual, cognitive and motor processors. It takes in account visual image, working memory and long term memory storages of users, thus also accounting for their cycle and decay times. After breaking down a process to basic actions, the Human Processor Model quantifies each action and attempts to determine the total time needed to complete the process. 

The model uses experimental cognitive and motor processing times, thus not needing any users to complete the process in order to approximate its time cost.

Figure \ref{fig:hmp} depicts the human processor model. Besides the three main processes which model how people perceive and remember information, the figure shows the experimental times for the required actions.

\begin{figure}[!h]
  \centering
    \includegraphics[width=0.5\textwidth]{abb/mhp.png}
      \caption{The Human Processor Model, a cognitive modelling method used to calculate the time needed to complete a task by drawing analogies between how the human brain stores and processes information to how a computer acts in the same situation \cite[Figure 1]{card1986model}.}
       \label{fig:hmp}
\end{figure}

\subsection{Hierarchical Task Analysis}

Hierarchical task analysis was developed in order to analyse complex and nonrepetitive cognitive tasks which can be broken into subtasks. Nowadays, it is used in various domains, including interface and process design \cite{annett2003hierarchical}.

Using hierarchical task analysis means decomposing a task into subtasks recursively, until the desired level of detail is reached. The goal of this analysis is to detect the sources of possible performance issues in a process, as well as discover subtasks or actions which can be optimised.

Multiple similar approaches to break tasks down were developed based on or related to hierarchical task analysis, such as GOMS.

\subsection{GOMS}

GOMS stands for "a set of Goals, a set of Operators, a set of Methods for achieving the Goals, and a set of Selection rules for choosing among competing methods for goals" \cite{card2018psychology}. GOMS models are closely related to hierarchical task analysis. Some research include GOMS as a form of hierarchical task analysis \cite{kirwan1992guide}. Despite this, GOMS models are more disciplined than hierarchical task analysis, since the latter just assume that tasks can be broken down in subtasks until the actions are atomic. GOMS focuses on achieving goals and subgoals during the process.

GOMS models describe analysed tasks in sequences of operations with goals.
Goals are symbolic states, which have to be achieved along the process. Operators are used to describe user actions and assign a cost to them and describe elementary perceptual, motor or cognitive tasks. Methods are procedures which lead to achieving a goal. Selection rules define which available method to complete a goal is used, since goals or subgoals may be achieved by requiring different sequences of operators \cite{kieras1988towards}.

\subsection*{GOMS Family}

GOMS has multiple variants, which are commonly known together as the GOMS models family \cite{john1996goms}.

\paragraph{CMN-GOMS.} CNM-GOMS is the first GOMS model. The syntax is similar to computer programs and breaks goals down into subgoals, which can be achieved using subroutines. Despite having some similarities to the human processor model, CMN-GOMS concentrates on users using operators to convert an initial state into a goal state. 

\paragraph{CPM-GOMS.} CPM-GOMS is an enhanced form of GOMS, which also takes in consideration parallelism. It uses the human processor model and adds activities to each different human processor model component, thus obtaining optimised execution times. However, tasks where few users can achieve parallelism because of its complexity or simple tasks are usually not modelled using CPM-GOMS because of the complexity this model generates.

\paragraph{NGOMSL.} NGOMSL, abbreviated from Natural GOMS Language model, is an extension to CMN-GOMS. It allows task modelling using natural language.

\paragraph{Keystroke Level Model.} The keystroke level model is much more restrictive than other GOMS variants, since there are no selection rules included. Therefore, experts have to decide upon the method of accomplishing the task before starting modelling with it.

\subsection*{Advantages}
There are multiple advantages to modelling and comparing processes using GOMS or other models from the GOMS family. 

Firstly, no users or user trials are needed. Since human processing models provide a form of quantisation, there is no need for large user groups to test modelled processes.

Secondly, a process modelled in GOMS is deterministic. Since every GOMS operator has exactly one cost assigned to it, modelled processes may only differ in their total cost, if their steps are different.

Thirdly, GOMS is a objective way to model a process. Subjective factors present in user trials can be eliminated through GOMS. 

Lastly, by using GOMS, the evaluator effect is not possible.
\newpage
\subsection*{Disadvantages}
Firstly, GOMS assumes that all users are expert users. This means that the assumed users do little to no errors and have an already well defined workflow within the software being modelled. 

Secondly, modelling with GOMS is strictly goal directed. Processes which are based on problem solving cannot be modelled in GOMS, since a model is linear and must have fixed start and end points.

Thirdly, GOMS is built on statistical averages.

Another disadvantage is that GOMS cannot model any type of error handling during a process, if the error is not a part of the modelled process. 

Lastly, modelling can be time consuming and GOMS cannot provide meaningful data if the process is not fully modelled. Therefore, using GOMS can mean a greater time investment at the beginning of handling a process. 

\subsection{Keystroke Level Model}
The keystroke level model predicts the time needed to complete a task successfully by splitting the process into single actions, called operators, which the user iteratively performs. In comparison to GOMS, the keystroke level model is easier to apply. This model also assumes that the method how the user attempts to achieve the task is already known \cite{Card1980}.

The keystroke level model contains six standard operator classes. All operators can be summed up and parametrized. The first operator, K, concerns itself with pressing a key or a button. The second operator, P, describes pointing with the mouse at a target. H stand for homing hands, while D parametrizes drawing. Another operator, M, describes the time needed for mental preparation. Lastly, R takes into account the system response time. These operators were combined to common user actions on computer interfaces.
Figure \ref{fig:klmoperators} shows the execution time needed for the six basic operators, as well as some composed execution times for common actions on interfaces, such as clicking a link or a button.

To support modelling touch interface interactions, a similar model called Touch Level Model exists, which is based on the same idea. 

\begin{figure}[H]
  \centering
    \includegraphics[width=1\textwidth]{abb/KLM.png}
    \caption[Table showing simple and composed keystroke level model operators, as well as their description and their time cost.]{Table showing simple and composed keystroke level model operators, as well as their description and their time cost\footnotemark.}
      \label{fig:klmoperators}
\end{figure}
\footnotetext{Keystroke Level Model article on Medium.com. \url{https://medium.com/enterprise-ux/key-stroke-level-model-213dd054e5c7}. Last accessed August 2019.}

\subsection*{Keystroke Level Model Family}
Since the keystroke level model is not able to depict the full range of current mobile tasks \cite{li2010extended}, most abbreviations of the keystroke level model consider mobile and touch interfaces. They have been created based on the keystroke level model or modified the keystroke level model for this purpose. 

Operator blocks were introduced in an extension model of the keystroke level model to allow sequences of operators to be used repetitively by experts
\cite{li2010extended}. Other adaptations of the keystroke level model introduced new operators, which should enhance the ability to model touch interface processes.